//
//  PageControllerVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 12/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import CarbonKit
class PageControllerVC: UIPageViewController,UIPageViewControllerDataSource {
    //MARK:- View Method
     weak var carbonNavigation : CarbonTabSwipeNavigation?
    lazy var  viewControllerList:[UIViewController] = {
        
        let ObjectMainSb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc1 = ObjectMainSb.instantiateViewController(withIdentifier: "v1")
        let vc2 = ObjectMainSb.instantiateViewController(withIdentifier: "v2")
        let vc3 = ObjectMainSb.instantiateViewController(withIdentifier: "v3")
        return [vc1,vc2,vc3]
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.dataSource = self
        
        if  let viewController1 =  self.viewControllerList.first {
            self.setViewControllers([viewController1], direction: .forward, animated: true, completion: nil)
        }
    }
    // PageConreoller Datasurce method
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        let nextIndex = vcIndex+1
        guard viewControllerList.count  != nextIndex else { return nil }
        
        guard viewControllerList.count > nextIndex else { return nil }
        return viewControllerList[nextIndex]
        
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        let previousIndex = vcIndex-1
        guard previousIndex >= 0 else {
            return nil
        }
        guard   self.viewControllerList.count > previousIndex  else { return nil }
        return viewControllerList[previousIndex]
      
    }
}
