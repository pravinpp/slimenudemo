//
//  ListContactVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 17/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class ListContactVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblMainVC: UITableView!
    
    var arrConact : [ContactNumber] = [ContactNumber]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        APIManagerData._APIManagerInstance.getAPI { (responseArr) in
            self.arrConact = responseArr.arrContact!
            self.tblMainVC.reloadData()
        }
    }
    //MARK: DataSource & delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrConact.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell  = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        cell.lblName.text = self.arrConact[indexPath.row].name
        cell.lblEmail.text = self.arrConact[indexPath.row].email
        cell.lblAddress.text = self.arrConact[indexPath.row].address
        cell.lblPhone.text = self.arrConact[indexPath.row].phone
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
 
}

