//
//  TabBarVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 17/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class TabBarVC: UIViewController {
    //MARK :- View Method
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.tabBarControllerVC()
        appDelegate.tabarVC.selectedIndex = 1
    }

}
