//
//  CollectionrDeatilsVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 11/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class CollectionrDeatilsVC: UIViewController {
    
    var image : UIImage = UIImage()
    var strfoodName : String = ""

    @IBOutlet weak var imageFood: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageFood.image = image
        self.lblName.text = strfoodName
        self.navigationController?.isNavigationBarHidden = false
        let leftbar = UIBarButtonItem.init(title: "Back", style: .plain, target: self, action: #selector(backMenu))
        self.navigationController?.navigationItem.leftBarButtonItem = leftbar
    }
     @objc func backMenu(){
        self.navigationController?.popViewController(animated: true)
    }
}
