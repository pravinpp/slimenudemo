//
//  customCell.swift
//  SlideMenu
//
//  Created by Dubond Mac on 11/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class customCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var imgFood: UIImageView!
    
    override func awakeFromNib() {
        self.imageView.layer.cornerRadius =  5
        self.imageView.clipsToBounds = true
        self.imageView.backgroundColor = UIColor.red

    }
}
