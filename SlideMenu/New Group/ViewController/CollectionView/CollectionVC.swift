//
//  CollectionVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 11/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import CarbonKit

class CollectionVC: UIViewController {
    
    weak var carbonNavigation : CarbonTabSwipeNavigation?
    var  arrImage:[UIImage] = [UIImage]()
    var  arrImageName = ["Food Past","Food Past2","Food Past3","Food Past4","Food Past5","Food Past6","Food Past7","Food Past8","Food Past9","Food Past10"]
    @IBOutlet weak var Collection1: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrImage = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5"),#imageLiteral(resourceName: "image6"),#imageLiteral(resourceName: "image6"),#imageLiteral(resourceName: "image8"),#imageLiteral(resourceName: "image9"),#imageLiteral(resourceName: "image10")]
        let leftbar = UIBarButtonItem.init(title: "Back", style: .plain, target: self, action: #selector(backMenu))
        navigationItem.leftBarButtonItem = leftbar
    }
    @objc func backMenu(){
        let CollectionVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier: "CollectionVC") as! CollectionVC
        let CollectionVCNavController = UINavigationController(rootViewController: CollectionVCInstnce)
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.centerViewController = CollectionVCNavController
        appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
}
//MARK:- Collection delegates & DataSource
extension CollectionVC :UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
        if kind == UICollectionElementKindSectionHeader{
            
            if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCell", for: indexPath) as? HeaderCell{
                sectionHeader.sectionHeaderlabel.text = "Hearder Section \(indexPath.section)"
                return sectionHeader
            }
        }
        else{
            if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCell", for: indexPath) as? FooterCell{
                sectionHeader.sectionHeaderlabel.text = "Footer Section \(indexPath.section)"
                return sectionHeader
            }
        }
        return UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return self.arrImage.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customCell", for: indexPath)  as! customCell
        cell.lblFoodName.text = self.arrImageName[indexPath.row]
        cell.imgFood.image = self.arrImage[indexPath.row]
        cell.imageView.layer.shadowColor = UIColor.black.cgColor
        cell.imageView.layer.shadowOpacity = 1
        cell.imageView.layer.shadowOffset = CGSize.init(width: 0, height: 1)
        cell.imageView.layer.shadowRadius = 1
        cell.imageView.layer.masksToBounds = false
        return cell
    }

    func   collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 152, height: 152)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
                let  nextvc = ObjectMainSB.instantiateViewController(withIdentifier:"CollectionrDeatilsVC") as! CollectionrDeatilsVC
                nextvc.image = self.arrImage[indexPath.row]
                nextvc.strfoodName = self.arrImageName[indexPath.row]
                self.navigationController?.pushViewController(nextvc, animated: true)
    }


    
}


