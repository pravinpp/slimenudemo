//
//  TableVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 12/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class TableVC: UIViewController {

    @IBOutlet var tblMainVC: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
extension TableVC: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       var cell = UITableViewCell()
        if indexPath.row == 0 {
           let cellp = configurPageCell(tableview: tableView, indexPath: indexPath)
            cell = cellp
           return cell
        }
        else{
            let  cellc  = configurCollectionCell(tableview: tableView, indexPath: indexPath)
            cell = cellc
            return cell
        }
       //return cell
    }
    func configurPageCell(tableview :UITableView, indexPath: IndexPath) -> PageCell {
         let cellP = tableview.dequeueReusableCell(withIdentifier:"PageCell") as! PageCell
         return cellP
    }
    func configurCollectionCell(tableview :UITableView, indexPath: IndexPath) -> CollectionCell {
        let cellP = tableview.dequeueReusableCell(withIdentifier:"CollectionCell") as! CollectionCell
        return cellP
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 350
        }
        else{
            return 120
        }
    }
}
