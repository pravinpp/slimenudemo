//
//  CollectionCell.swift
//  SlideMenu
//
//  Created by Dubond Mac on 12/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class CollectionCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
   
    

    @IBOutlet var Collection: UICollectionView!
    var  arrImage:[UIImage] = [UIImage]()
    var  arrImageName = ["Food Past","Food Past2","Food Past3","Food Past4","Food Past5","Food Past6","Food Past7","Food Past8","Food Past9","Food Past10"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // 
        self.arrImage = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5"),#imageLiteral(resourceName: "image10"),#imageLiteral(resourceName: "image6"),#imageLiteral(resourceName: "image8"),#imageLiteral(resourceName: "image9"),#imageLiteral(resourceName: "image10")]
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    //CollectionVC
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImage.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"CollectonVCCell1", for: indexPath) as! CollectonVCCell1
        cell.lblName.text = self.arrImageName[indexPath.row]
        cell.imgFood.image = self.arrImage[indexPath.row]
        cell.imgFood.layer.cornerRadius = cell.frame.size.height / 2.0
        cell.imgFood.clipsToBounds = true
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }

}
