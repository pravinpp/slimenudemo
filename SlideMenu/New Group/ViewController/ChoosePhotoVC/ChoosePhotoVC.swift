//
//  ChoosePhotoVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 19/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import Parse

class ChoosePhotoVC: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
  
    @IBOutlet weak var imgPhoto: UIImageView!
    //MARK:-  UIView Method
    let  picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK;- Button Action Method
    func fetchDataPhotoClass(){
        let  query = PFQuery.init(className: "Photo")
        query.findObjectsInBackground { (arrResponse, error) in
            if error == nil {
                
                for item in arrResponse! {
                    print("\(item)")
                    
                    let title = item["title"]  as? String
                    print(" Title ==> \(title)")
                    
                }
            }
        }
    }
    @IBAction func OnClickChooseImage(_ sender: Any) {

        let alertVC = UIAlertController.init(title: "Choose Photo", message: "", preferredStyle: .actionSheet)
        
        let ChoosePhoto = UIAlertAction.init(title: "Choose Gallary", style: .default) { (choosegally) in
            self.chosePhoto()
        }
        let camera = UIAlertAction.init(title: "Camera", style: .default) { (camera) in
            
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (cancel) in
            
        }
        alertVC.addAction(ChoosePhoto)
        alertVC.addAction(camera)
        alertVC.addAction(cancel)
        self.present(alertVC, animated: true, completion: nil)

    }
    func chosePhoto() {
        
        self.picker.delegate = self
        self.picker.allowsEditing =  true
        self.picker.sourceType = .photoLibrary
        self.present(self.picker, animated: true, completion: nil)
    }
    //MARK:- ImagePickerDelegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let img =  info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imgPhoto.image = img
        DispatchQueue.main.async {
            self.insertRecordParseDatabse(image: img)
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
    }
    func insertRecordParseDatabse(image: UIImage) {
        let photo = PFObject(className: "Photo")
        let imageData = UIImageJPEGRepresentation(image, 0.2)
        let imageFile = PFFile(name: "test.png", data: imageData!)
        photo["nameimage"]  = "Uplaod image 1"
        photo["title"]  = "Animal Photo"
        photo["image"] = imageFile
        photo.saveInBackground { (success, error ) -> Void in
            if success {
                print("Photo uploaded")
            }
            else {
                print("ERROR   =>\(error.debugDescription)")
            }
        }
    }
}
