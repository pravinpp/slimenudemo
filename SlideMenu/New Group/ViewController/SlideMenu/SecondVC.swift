//
//  SecondVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 07/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class SecondVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
   // var tblMainvc : CustomTable!
    @IBOutlet   var tblcustom: UITableView!
    var  arrData = NSMutableArray ()
    var  expandArrData = [NSMutableDictionary]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.expandArrData.append(["isOpen":"1","key":"Fruit","Data":["Banana","Mango","Orange"]])
        self.expandArrData.append(["isOpen":"1","key":"Vagitable","Data":["Potato","Tomato"]])
        self.expandArrData.append(["isOpen":"1","key":"Animal","Data":["Dog","Monkey","Rabbit"]])
        
//        let left = UIBarButtonItem.init(title: "Open", style: .plain, target: self, action: #selector(openLeft))
//        self.navigationItem.leftBarButtonItem = left
        self.title = "SecondVC"
        self.arrData = ["A","B","C","D","E","F","G"]
        //
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            print("Call 5 second =======>")
           // self.tblcustom.arrDataTemp = self.expandArrData
           // self.tblcustom.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }

    @objc func openLeft(){
        
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    //
    @objc func  tapGesture(sender: UITapGestureRecognizer, tableview: UITableView) {
        //print("hearde tap")
        if (self.expandArrData[(sender.view?.tag)!].value(forKey: "isOpen") as! String == "1") {
            self.expandArrData[(sender.view?.tag)!].setValue("0", forKey: "isOpen")
        }else {
            self.expandArrData[(sender.view?.tag)!].setValue("1", forKey: "isOpen")
        }
        self.tblcustom.reloadSections(IndexSet(integer: sender.view!.tag), with: .automatic)
    }
    
    func  numberOfSections(in tableView: UITableView) -> Int {
        return self.expandArrData.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerview = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 70))
        let lblname = UILabel.init(frame: CGRect.init(x: 10, y: 5, width: headerview.frame.size.width-50, height: 20))
        lblname.font = UIFont.systemFont(ofSize: 18)
        //let dict = self.arrDataTemp as! NSDictionary
        lblname.text =  self.expandArrData[section].value(forKey: "key") as? String
        headerview.addSubview(lblname)
        headerview.tag = section
        let tap = UITapGestureRecognizer.init(target: self, action:#selector(tapGesture(sender:tableview:)))
        headerview.addGestureRecognizer(tap)
        headerview.backgroundColor = UIColor.lightGray
        return headerview
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.expandArrData[section].value(forKey: "isOpen") as! String  == "1" {
            return 0
        }
        else {
            let arrData = self.expandArrData[section].value(forKey: "Data")  as! NSArray
            return arrData.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SecondCell") as! SecondCell
        let  arrsection = self.expandArrData[indexPath.section].value(forKey: "Data") as! NSArray
        cell.lblName.text = arrsection[indexPath.row] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let nextVC = ObjectMainSB.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}
