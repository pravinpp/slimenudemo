//
//  MenuListVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 07/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class MenuListVC: UIViewController {
    //
    var arrMenu =  NSMutableArray()
    @IBOutlet weak var MainTableVC: UITableView!
    //MARK:- UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        if let drawerController = parent as? KYDrawerController {
            if drawerController.drawerState == .opened
            {
                //drawerController.setDrawerState(.closed, animated: true)
                drawerController.setDrawerState(.opened, animated: true)
            }
        }
        //KYDrawerController.setDrawerState()
        self.arrMenu = ["","CollectionVC","TableView","PageController","Autolayout","Notification","CardView","View Pager","Google Map","Button Action","SQliteDatabse","Parse  Login","Popup View","CollectionView Cell Dynamic","Location Service On","Login View","Expandable Tableview"]
        self.title = "MENU PAGE"
        self.navigationController?.isToolbarHidden = false
        self.MainTableVC.rowHeight = 200
        self.MainTableVC.estimatedRowHeight =  UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.view.layoutSubviews()
    }
}
//MARK:- Tableview DataSource & Delegate
extension MenuListVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMenu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.row == 0 {
            let  cellp = configureProfilecell(tableView: tableView, indexPath: indexPath)
            cell = cellp
        }
        else {
           let cellm = configureMenucell(tableView: tableView, indexPath: indexPath)
            cell = cellm
        }
        return cell
    }
    
    func configureProfilecell(tableView: UITableView, indexPath: IndexPath)  -> ProileCell {
        let pcell = tableView.dequeueReusableCell(withIdentifier: "ProileCell") as! ProileCell
        return pcell
    }
    func configureMenucell(tableView: UITableView, indexPath: IndexPath)  -> MenuCell {
        let mcell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        mcell.lblTitle.text = self.arrMenu[indexPath.row] as? String
        return mcell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {

            let CollectionVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier: "CollectionVC") as! CollectionVC
            let CollectionVCNavController = UINavigationController(rootViewController: CollectionVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = CollectionVCNavController
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)

        }
        else if indexPath.row == 2  {
            
            let SecondVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier: "TableVC") as! TableVC
            let SecondVCNavController = UINavigationController(rootViewController: SecondVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = SecondVCNavController
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 3 {
            let PageControllerVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier:"PageControllerVC") as! PageControllerVC
            let PageControllerVCNavController = UINavigationController(rootViewController: PageControllerVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = PageControllerVCNavController
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 4 {
            let autolayOutVCVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier:"AutolayOutVC") as! AutolayOutVC
            let autolayOutVCNavigation = UINavigationController(rootViewController: autolayOutVCVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = autolayOutVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 5 {
            let notificationVCVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier:"NotificationVC") as! NotificationVC
            let notificationVCNavigation = UINavigationController(rootViewController: notificationVCVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = notificationVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 6 {
            let CardLayOutVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier:"CardLayOutVC") as! CardLayOutVC
            let CardLayOutVCNavigation = UINavigationController(rootViewController: CardLayOutVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = CardLayOutVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 7 {
            let actionButtonVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier:"PageViewVC") as! PageViewVC
            let actionButtonVCNavigation = UINavigationController(rootViewController: actionButtonVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = actionButtonVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 8 {
            let GoogleMapVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier:"GoogleMapVC") as! GoogleMapVC
            let GoogleMapVCInstnceNavigation = UINavigationController(rootViewController: GoogleMapVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = GoogleMapVCInstnceNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 9 {
            let actionButtonVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier:"PageViewVC") as! PageViewVC
            let actionButtonVCNavigation = UINavigationController(rootViewController: actionButtonVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = actionButtonVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 10 {
            let actionButtonVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier:"SqLiteDatabaseVC") as! SqLiteDatabaseVC
            let actionButtonVCNavigation = UINavigationController(rootViewController: actionButtonVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = actionButtonVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 11 {
            let actionButtonVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier:"ParseLoginVC") as! ParseLoginVC
            let actionButtonVCNavigation = UINavigationController(rootViewController: actionButtonVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = actionButtonVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 12 {
            let actionButtonVCInstnce =  ObjectDesignSB.instantiateViewController(withIdentifier:"DesignVC") as! DesignVC
            let actionButtonVCNavigation = UINavigationController(rootViewController: actionButtonVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = actionButtonVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 13 {
            let actionButtonVCInstnce =  ObjectDesignSB.instantiateViewController(withIdentifier:"CollectionVCExpand") as! CollectionVCExpand
            let actionButtonVCNavigation = UINavigationController(rootViewController: actionButtonVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = actionButtonVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 14 {
            let GPS_VCInstnce =  ObjectDesignSB.instantiateViewController(withIdentifier:"GPS_VC") as! GPS_VC
            let GPS_VCNavigation = UINavigationController(rootViewController: GPS_VCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = GPS_VCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 15 {
            let LoginVCInstnce =  ObjectDesignSB.instantiateViewController(withIdentifier:"LoginVC") as! LoginVC
            let LoginVCNavigation = UINavigationController(rootViewController: LoginVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = LoginVCNavigation
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else {
          
            let SecondVCInstnce =  ObjectMainSB.instantiateViewController(withIdentifier: "SecondVC") as! SecondVC
            let SecondVCNavController = UINavigationController(rootViewController: SecondVCInstnce)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = SecondVCNavController
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }

      
        
    }
    
}
