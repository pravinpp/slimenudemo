//
//  CustomTable.swift
//  SlideMenu
//
//  Created by Dubond Mac on 10/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

/*class CustomTable: UITableView,UITableViewDataSource,UITableViewDelegate {
    //
     var  arrDataTemp = [NSMutableDictionary]()
    //override  var style:
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
    }
    override func awakeFromNib() {
        self.delegate  = self
        self.dataSource = self
    }
    //
 
    //
    @objc func  tapGesture(sender: UITapGestureRecognizer, tableview: UITableView) {
        print("hearde tap")
        if (self.arrDataTemp[(sender.view?.tag)!].value(forKey: "isOpen") as! String == "1") {
            self.arrDataTemp[(sender.view?.tag)!].setValue("0", forKey: "isOpen")
        }else {
            self.arrDataTemp[(sender.view?.tag)!].setValue("1", forKey: "isOpen")
        }
        tableview.reloadSections(IndexSet(integer: sender.view!.tag), with: .automatic)
       tableview.reloadData()
    }
    func  numberOfSections(in tableView: UITableView) -> Int {
        return self.arrDataTemp.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //
        let headerview = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
        let lblname = UILabel.init(frame: CGRect.init(x: 10, y: 5, width: headerview.frame.size.width-50, height: 20))
        lblname.font = UIFont.systemFont(ofSize: 18)
        //let dict = self.arrDataTemp as! NSDictionary
        lblname.text =  self.arrDataTemp[section].value(forKey: "key") as? String
        headerview.addSubview(lblname)
        headerview.tag = section
        let tap = UITapGestureRecognizer.init(target: self, action:#selector(tapGesture(sender:tableview:)))
        headerview.addGestureRecognizer(tap)
        return headerview
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if self.arrDataTemp[section].value(forKey: "isOpen") as! String  == "1" {
            return 0
        }
        else {
            let arrData = self.arrDataTemp[section].value(forKey: "Data")  as! NSArray
            return arrData.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "SecondCell") as! SecondCell
        let  arrsection = self.arrDataTemp[indexPath.section].value(forKey: "data") as! NSArray
        cell.lblName.text = arrsection[indexPath.row] as? String
        return cell
    }
    
    
}*/
