//
//  ViewController.swift
//  SlideMenu
//
//  Created by Dubond Mac on 07/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
  let appDelegate = UIApplication.shared.delegate as! AppDelegate

class ViewController: UIViewController {
    
    @IBOutlet weak var BgView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let  testObject = Test ()
        let testSubInstance = TestSub()
        print(testSubInstance.city)
        print(testSubInstance.name)
        
        self.BgView.layer.cornerRadius = 15
        self.BgView.clipsToBounds = true
        self.BgView.backgroundColor = UIColor.init(patternImage: UIImage.init(named:"image1")!)
        
        //Shadow
        //gradient
//        let gradient: CAGradientLayer = CAGradientLayer()
//        gradient.colors = [UIColor.blue.cgColor, UIColor.yellow.cgColor]
//        gradient.locations = [0.0 , 1.0]
//        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
//        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
//        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.BgView.frame.size.width, height: self.BgView.frame.size.height)
//        self.BgView.layer.insertSublayer(gradient, at: 0)
        //gradeint
//        let gradientView = UIView(frame: CGRect(x: 0, y: 0, width: self.BgView.bounds.width, height: self.BgView.frame.size.height))
//        let gradientLayer:CAGradientLayer = CAGradientLayer()
//        gradientLayer.frame.size = gradientView.frame.size
//        gradientLayer.colors =
//            [UIColor.magenta.cgColor,UIColor.purple.cgColor]
//        BgView.layer.addSublayer(gradientLayer)
        
//        self.BgView.layer.shadowPath = UIBezierPath.init(roundedRect: self.BgView.bounds, cornerRadius: self.BgView.layer.cornerRadius).cgPath
//        self.BgView.layer.shadowColor = UIColor.green.cgColor
//        self.BgView.layer.shadowOffset = CGSize.init(width: -1.0, height: 1.0)
//        self.BgView.layer.shadowOpacity = 0.9
//        self.BgView.layer.shadowRadius = 1
//        self.BgView.layer.masksToBounds = false
        
        self.BgView.layer.shadowColor = UIColor.red.cgColor
        self.BgView.layer.shadowOpacity = 0.5
        self.BgView.layer.shadowOffset = CGSize.zero
        self.BgView.layer.shadowRadius = 2
        self.BgView.layer.masksToBounds = false
        
        let leftbar = UIBarButtonItem.init(title: "Open", style: .plain, target: self, action: #selector(openLeft))
        navigationItem.leftBarButtonItem = leftbar
        self.title = "HOME PAGE"
          let p1 = person()
          p1.name = "ppppppppppp"
          print("Person==\(p1.name)")
        DispatchQueue.main.asyncAfter(deadline: .now()+5) {
            print("5 second  after .....")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-
    @objc func openLeft(){
        
        let ViewControllerVC =  ObjectMainSB.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let aboutNavController = UINavigationController(rootViewController: ViewControllerVC)
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.centerViewController = aboutNavController
        appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)

    }
}
class  person {
    
    private var  _name : String?
    private var sname = ""
     //init()
    var name :String {
        get {
            if _name == "" {
            }
            return _name!
        }
        set {
            return _name  = newValue
        }
    }
}
class Test {
    var name = "Test"
    final var city = "Rajkot"
     func swipe(){
        
    }
}
class  TestSub: Test {
   
    override init() {
        super.init()
        name = "PravinParmar"
    }
    override func swipe() {
        
    }

    
    
}


