//
//  NotificationVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 14/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import UserNotifications
class NotificationVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       // notificationAdd()
        
    }
    
    func notificationAdd(){
        
        let center = UNUserNotificationCenter.current()
        let likeAction = UNNotificationAction(identifier: "like", title: "Like", options: [])
        let localCat =  UNNotificationCategory(identifier: "category", actions: [likeAction], intentIdentifiers: [], options: [])

        let content = UNMutableNotificationContent()
        content.title = "Warning"
        content.body = " Are you sure want to delete?"
        content.categoryIdentifier = "Alarm"
        content.userInfo = ["name22":"jay"]
        content.subtitle = " sub  name Jay "
        content.badge = 1
        
        //download.jpeg
        
//        let url1 = Bundle.main.path(forResource: "download", ofType: "jpeg")
//        do {
//            let content = try String(contentsOfFile:url1!, encoding: String.Encoding.utf8)
//            print(content)
//        } catch {
//            print("nil")
//        }
        let url1 =  Bundle.main.url(forResource: "download", withExtension: "jpeg")
        let attachment = try! UNNotificationAttachment(identifier: "image", url: url1!, options: [:])
        content.attachments = [attachment]
       //
        content.sound = UNNotificationSound.default()
//        var datecomponentInstance = DateComponents()
//        datecomponentInstance.second = 5
        UNUserNotificationCenter.current().setNotificationCategories([localCat])

        let  triger2 = UNTimeIntervalNotificationTrigger.init(timeInterval: 60.0, repeats: true)
        //let trigger = UNCalendarNotificationTrigger.init(dateMatching: datecomponentInstance, repeats: true)
        let request  =  UNNotificationRequest(identifier: "localnotification" , content: content, trigger: triger2)
        center.add(request) { (error) in
            print("==> \(String(describing: error))")
        }
    }
    //

}
