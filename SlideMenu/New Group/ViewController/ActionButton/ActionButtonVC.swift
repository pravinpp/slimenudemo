//
//  ActionButtonVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 25/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class ActionButtonVC: UIViewController {

    var actionButton :ActionButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.actionButtonAddMainview()
        
    }
    func actionButtonAddMainview(){
        
        let imgtwitter = UIImage(named: "twitter_icon")
        let imgGoogle = UIImage(named: "googleplus_icon")
        let btnTwitter = ActionButtonItem(title: "Twitter", image: imgtwitter)
        btnTwitter.action = { Item in
            print("Twitter")
        }
        let btnGooogle = ActionButtonItem(title: "Google", image: imgGoogle)
        btnGooogle.action = { item in
            print("Google")
            let nextVC = ObjectMainSB.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
        self.actionButton =  ActionButton.init(attachedToView: self.view, items: [btnGooogle,btnTwitter])
        self.actionButton.action = { bottom in bottom.toggleMenu() }
        self.actionButton.setTitle("+", forState: .normal)
        self.actionButton.backgroundColor = UIColor.orange

        
        
    }

}
