//
//  GPS_VC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 17/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import CoreLocation

class GPS_VC: UIViewController,CLLocationManagerDelegate {
    
    let  locationManager = CLLocationManager()
    
    //MARK:- UIView Method
    override func viewDidLoad() {
      super.viewDidLoad()
        
        //
        
         #if Live
            print("Live...")
         #else
            print("Debug........")
         #endif
        
        //
        locationManager.requestAlwaysAuthorization()
        if  CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate =     self
            locationManager.startUpdatingLocation()
        }
        
    }
    
    
    @IBAction func OnClickGPSLocation(_ sender: Any) {
        
        locationManager.requestAlwaysAuthorization()
        if  CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate =     self
            locationManager.startUpdatingLocation()
            
        }
    }
    //MARK:- Core Location Delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let  location = locations.first
        print(" ==> \(location?.coordinate)")
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.denied {
           alertShow()
        }
    }
    //MARK:- Custom Method
    func  alertShow(){
        
        let  aletVC = UIAlertController.init(title: "Location", message: " On LocationServiece", preferredStyle: .alert)
       // let actionOk  =  UIAlertAction.init(title: "", style: .default) { (Ok) in
            let settingsAction = UIAlertAction(title: "Go to Setting", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: "App-Prefs:root=Privacy") else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    
                    UIApplication.shared.open(settingsUrl, options: [:], completionHandler: { (success) in
                        print("Success ....")
                    })
                }
//                if UIApplication.shared.canOpenURL(settingsUrl) {
//                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                        // Checking for setting is opened or not
//                        print("Setting is opened: \(success)")
//                    })
//                }
            }
            // let  url =
        //}
        aletVC.addAction(settingsAction)
        self.present(aletVC, animated: true, completion: nil)
    }
}
