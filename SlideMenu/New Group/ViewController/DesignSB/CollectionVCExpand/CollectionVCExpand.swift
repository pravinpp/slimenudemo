//
//  CollectionVCExpand.swift
//  SlideMenu
//
//  Created by Dubond Mac on 15/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class CollectionVCExpand: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
 
    
    var arrTitle = ["test", "test1","Test3"]
    var arrImage:[UIImage] = [UIImage]()

    @IBOutlet weak var CollectionVC: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrImage = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5"),#imageLiteral(resourceName: "loc"),#imageLiteral(resourceName: "image6"),#imageLiteral(resourceName: "image8"),#imageLiteral(resourceName: "image9"),#imageLiteral(resourceName: "image10")]
    
    }
    //MARK:- CollectionView DataSource & delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImage.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExpandlleCell", for: indexPath) as! ExpandlleCell
        cell.img.image = self.arrImage[indexPath.row]
        cell.img.layer.cornerRadius = 10
        cell.img.clipsToBounds = true
       
        return cell
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        let width  = (view.frame.width-30)/2
//        let img = self.arrImage[indexPath.row]
//        let height = img.size.height+20
//        print("height ==>\(height)")
//       return CGSize(width: width, height: height)
//    }
 
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return  UIEdgeInsetsMake(10, 10, 10, 10)
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//       return  5
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
    
//        let width = (self.view.frame.size.width - 20) / 3 //some width
//        let height = width * 1.5 //ratio
//        return CGSize(width: width, height: height)
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let numberOfCell: CGFloat = 3   //you need to give a type as CGFloat
//        let cellWidth = UIScreen.main.bounds.size.width / numberOfCell
//        return CGSize.init(width: cellWidth, height: cellWidth)
//    }

}

