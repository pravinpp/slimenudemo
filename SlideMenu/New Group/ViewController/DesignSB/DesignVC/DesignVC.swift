//
//  DesignVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 10/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class DesignVC: UIViewController {
    //MARK:- UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.view = lazyPropertyView
        //self.view = closureView()
    }
    
    var lazyPropertyView : UIView = {
        let v1 = UIView()
        v1.backgroundColor = UIColor.green
        return v1
    }()
    let closureView = { () -> UIView in
         let v1 = UIView()
         v1.backgroundColor = UIColor.red
         return v1
    }
    @IBAction func OnClickPush(_ sender: Any) {
        let nextVc = ObjectDesignSB.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
        self.present(nextVc, animated: false, completion: nil)
    }
    @IBAction func OnClickExpandable(_ sender: Any) {
        
        let nextVc = ObjectDesignSB.instantiateViewController(withIdentifier:"TableCellExpandable") as! TableCellExpandable
        
        self.present(nextVc, animated: false, completion: nil)
    }
    
}
