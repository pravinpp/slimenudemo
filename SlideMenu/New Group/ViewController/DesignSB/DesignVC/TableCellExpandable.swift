//
//  TableCellExpandable.swift
//  SlideMenu
//
//  Created by Dubond Mac on 11/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class  shape {
    
    var area: Double?
    func  calculateArea(valueA:Double,valueB:Double) -> Void {
        print(" Call Shape ")
    }
    func calculateArea(a:Double,b:Double,c:Double) -> Void {
        
         let cal = a*b*c
        print(" call Shap a,b,c  => \(cal) ")
    }
    
}
class Rectangle: shape {
    override func calculateArea(valueA A: Double, valueB B: Double) {
         area =  A * B
        print("call Rectegale Area :- \(area)")
    }
}

class TableCellExpandable: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
    
    @IBOutlet var TblMainVC: UITableView!
    
    var allData :[ModelData] = [ModelData]()
    
    lazy var  name: String = {  [unowned self] in
        
        return "Hello World "
    }()
    //MARK:- UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let  rect = Rectangle()
          rect.calculateArea(valueA: 10, valueB: 50)
           rect.calculateArea(a: 20, b: 30, c: 10)
        
        let  num1 = 100
        let  num2 = 20
        let max = num1 > num2 ? num1 : num2
        print("Max Number :- \(max)")
        self.TblMainVC.estimatedRowHeight = 82
        self.TblMainVC.rowHeight =  UITableViewAutomaticDimension
    // arrayDataSource
    self.allData = [ModelData.init(title: "Vestibulum id ligula porta felis euismod semper.",subtitle: "Cum sociis natoque penatibus et magnis dis parturient montes,nascetur ridiculus mus. Maecenas sed diam eget risus varius blandit sit amet non magna.", isExpande: false),ModelData.init(title: "Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis .", subtitle: "Etiam porta sem malesuada magna mollis euismod. Nullam quis risus urna mollis ornare vel eu leo.", isExpande: false),ModelData.init(title: "Aenean lacinia bibendum nulla sed consectetur.", subtitle: "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus.", isExpande: false)]
    }
    //MARK:- Tableview DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int  {
         return  self.allData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"ExpandableCell2") as!  ExpandableCell2
        cell.setData(Object: self.allData[indexPath.row])
//          let object = self.allData[indexPath.row]
//          cell.lblTitle.text = object.title
//          cell.lblSubTitle.text = object.isExpande ? object.subtitle: ""
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = self.allData[indexPath.row]
        if !object.isExpande {
            object.isExpande = true
        }
        else{
            object.isExpande = false
        }
         // object.isExpande = !object.isExpande
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableViewAutomaticDimension
    }
}
