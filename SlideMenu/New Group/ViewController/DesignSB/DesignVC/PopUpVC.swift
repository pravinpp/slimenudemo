//
//  PopUpVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 10/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class PopUpVC: UIViewController {

    @IBOutlet weak var ContainerVC: UIView!
    //
    
    
    var lazyPropertyView : UIView = {
         let v1 = UIView()
         v1.backgroundColor = UIColor.green
        return v1
    }()
    
    let setupViewClosure = {()->UIView in
      let v1 = UIView()
      v1.backgroundColor = UIColor.red
      return v1
    }
    var  callHello : () -> String  = {
       return "Hello ...."
    }
    var sum:(Int,Int) -> Int = {$0+$1}
    func returnClosure() -> (Int,Int) ->Int{
         return sum
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print("===>\(callHello())")
        print(returnClosure()(20,10))
        self.ContainerVC.layer.cornerRadius = 20
        self.ContainerVC.clipsToBounds = true
        setupViewframe()
    }
    
    @IBAction func OnClickCountinue(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    func setupViewframe(){
        var  v1 = UIView()
          v1.frame = CGRect.init(x:0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        print("==>\(v1)")
    }
    


}
