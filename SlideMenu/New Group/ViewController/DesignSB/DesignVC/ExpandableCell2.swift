//
//  ExpandableCell2.swift
//  SlideMenu
//
//  Created by Dubond Mac on 11/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class ExpandableCell2: UITableViewCell {

    @IBOutlet var lblSubTitle: UILabel!
    @IBOutlet var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(Object:ModelData){
        self.lblTitle.text = Object.title
        self.lblSubTitle.text = Object.isExpande ? Object.subtitle: ""
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
class ModelData{

    var title: String?
    var subtitle: String?
    var isExpande: Bool
    //
    init(title:String,subtitle:String, isExpande:Bool) {
    self.title = title
    self.subtitle = subtitle
    self.isExpande = isExpande
    }
}
