//
//  LoginVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 29/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    @IBOutlet weak var LoginView: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var btnTWitter: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    
    @IBOutlet weak var passwordVC: UIView!
    @IBOutlet weak var EmailVC: UIView!
    //MARK:- UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        self.imgBackground.image = UIImage.init(named:"image1")
        self.LoginView.layer.cornerRadius = 20
        self.LoginView.clipsToBounds = true
        //
        self.title = "Login"
        btnFacebook.layer.cornerRadius = 5
        btnFacebook.clipsToBounds = true
        btnTWitter.layer.cornerRadius = 5
        btnTWitter.clipsToBounds = true
        btnSignIn.layer.cornerRadius = 5
        btnSignIn.clipsToBounds = true
        //
        self.EmailVC.layer.cornerRadius = 10
        self.EmailVC.layer.borderColor = UIColor.lightGray.cgColor
        self.EmailVC.layer.borderWidth = 2
        self.EmailVC.clipsToBounds = true
        
        self.passwordVC.layer.cornerRadius = 10
        self.passwordVC.layer.borderWidth = 2
        self.passwordVC.layer.borderColor = UIColor.lightGray.cgColor
        self.passwordVC.clipsToBounds = true
        

    }


}
