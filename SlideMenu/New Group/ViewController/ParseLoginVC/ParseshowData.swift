//
//  ParseshowData.swift
//  SlideMenu
//
//  Created by Dubond Mac on 09/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import Parse

private  let ParseCellID = "ParseCell"

class ParseshowData: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var arrAllData:[PFObject] = [PFObject]()
    var object = PFObject.init(className: "Photo")

    //
    @IBOutlet var tblMainVC: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //Fetch Record
        fetchDataPhotoClass()
    }
    //MARK: Tableview DataSource & delegates
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAllData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:ParseCellID) as! ParseCell
        let object = self.arrAllData[indexPath.row]
        if let title = object["title"] as? String{
            cell.lblTitle.text = title
        }
        if let name = object["nameimage"] as? String{
            cell.lblImage.text = name
        }
        if let image = object["image"] as? PFFile{
            // let imageFile =
            image.getDataInBackground { (data, error) in
                let imageData = UIImage.init(data: data!)
                cell.imgName.image = imageData
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  92
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextvc = ObjectMainSB.instantiateViewController(withIdentifier: "EditParseData") as! EditParseData
        nextvc.object = self.arrAllData[indexPath.row]
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
   
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
             object = self.arrAllData[indexPath.row]
             self.deleteRecord(Object: object)
            self.arrAllData.remove(at: indexPath.row)
            let indexPath = IndexPath(item: indexPath.row, section: 0)
            //tableView.reloadRows(at: [indexPath], with: .top)
        }
        
    }
    func deleteRecord(Object:PFObject){
        
        object.deleteInBackground { (isDelete, error) in
            if isDelete {
                print("Delete Record successfully")
            }
            else {
                print("  => \(error.debugDescription)")
            }
            self.tblMainVC.reloadData()
        }
    }
    //MARK:- Fetch All Data
    func fetchDataPhotoClass() {
        let  query = PFQuery.init(className: "Photo")
         query.findObjectsInBackground { (arrResponse, error) in
            if error == nil {
                //self.arrAllData.addObjects(from: arrResponse!)
                self.arrAllData = arrResponse!
                print(" ==> \(self.arrAllData)")
                for item in arrResponse! {
                    print("\(item.objectId)")
                    let title = item["title"]  as? String
                    print(" Title ==> \(title)")
                }
            }
            DispatchQueue.main.async {
                self.tblMainVC.reloadData()
            }
        }
    }


}
