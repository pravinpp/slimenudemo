//
//  ParseCell.swift
//  SlideMenu
//
//  Created by Dubond Mac on 09/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class ParseCell: UITableViewCell {

    @IBOutlet weak var imgName: UIImageView!
    @IBOutlet weak var lblImage: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
