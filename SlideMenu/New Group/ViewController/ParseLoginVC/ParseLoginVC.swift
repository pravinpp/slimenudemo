//
//  ParseLoginVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 08/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import Parse


class ParseLoginVC: UIViewController {
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtusername2: UITextField!
    @IBOutlet weak var txtPassword2: UITextField!
    
    // MARK:- UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        let notificationButton = SSBadgeButton()
        notificationButton.frame = CGRect(x: 10, y: 10, width: 44, height: 44)
        notificationButton.setImage(UIImage(named:"cart")?.withRenderingMode(.alwaysOriginal), for: .normal)
        notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 20)
        notificationButton.badge = "10"
        
        let leftbtn = UIBarButtonItem(customView: notificationButton)
       self.navigationItem.setRightBarButton(leftbtn, animated: true)
        
        
    
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        let currentUser = PFUser.current()
        if currentUser != nil {
            
            
        }
      
    }
    //MARK:-  OnClick Method
    @IBAction func OnClickLogout(_ sender: Any) {
        logout()
    }
    @IBAction func OnClickSignIn(_ sender: Any) {
        
        PFUser.logInWithUsername(inBackground: txtUserName.text!, password: txtPassword.text!) { (user, error) in
            print( "User login successfully \(user.debugDescription)")
            if error == nil {
                
                self.txtPassword.text = ""
                self.txtPassword2.text = ""
                self.txtusername2.text = ""
                self.txtUserName.text = ""
                let alertVC = UIAlertController.init(title: "", message: "Login successfully", preferredStyle: .alert)
                let Ok = UIAlertAction.init(title: "Ok", style: .default) { (Ok) in
                    
                }
                let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (cancel) in
                }
                alertVC.addAction(Ok)
                alertVC.addAction(cancel)
                self.present(alertVC, animated: true, completion: nil)
            }
            else {

                let alertVC = UIAlertController.init(title: "Login Error", message: error?.localizedDescription, preferredStyle: .alert)
               
                let Ok = UIAlertAction.init(title: "Ok", style: .default) { (Ok) in
                    
                }
                alertVC.addAction(Ok)
                self.present(alertVC, animated: true, completion: nil)
            }
        }
    }
    @IBAction func OnClickSignUP(_ sender: Any) {
        
        let user = PFUser()
        user.username = txtusername2.text
        user.password = txtPassword2.text
        user.signUpInBackground { (isSuccess, error) in
            if isSuccess {
                print("SignUp success fully")
                self.txtPassword.text = ""
                self.txtPassword2.text = ""
                self.txtusername2.text = ""
                self.txtUserName.text = ""
                print( "User login successfully \(user.debugDescription)")
                let alertVC = UIAlertController.init(title:"", message: "Successfully", preferredStyle: .alert)
                let Ok = UIAlertAction.init(title: "Ok", style: .default) { (Ok) in
                    
                }
                let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (cancel) in
                    
                }
                alertVC.addAction(Ok)
                alertVC.addAction(cancel)
                self.present(alertVC, animated: true, completion: nil)
            }
            else{
                print("error :- \(error.debugDescription)")
            }
        }

    }
    @IBAction func OnClickShowData(_ sender: Any) {
        
        let next =  ObjectMainSB.instantiateViewController(withIdentifier: "ParseshowData") as! ParseshowData
        self.navigationController?.pushViewController(next, animated: true)
        
    }
    func  logout() {
        
        PFUser.logOutInBackground { (error) in
            print("==>\(error.debugDescription)")
            print("Logout")
        }
        
    }
    func fbLogin(){
        

    }
}
