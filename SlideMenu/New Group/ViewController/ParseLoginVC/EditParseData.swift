//
//  EditParseData.swift
//  SlideMenu
//
//  Created by Dubond Mac on 09/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import Parse

class EditParseData: UIViewController {

    @IBOutlet var imageName: UIImageView!
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var txtName: UITextField!
    var object = PFObject.init(className: "Photo")
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let title = object["title"] as? String{
           self.txtTitle.text = title
        }
        if let name = object["nameimage"] as? String{
           txtName.text = name
        }
        if let image = object["image"] as? PFFile{
            // let imageFile =
            image.getDataInBackground { (data, error) in
                let imageData = UIImage.init(data: data!)
               self.imageName.image = imageData
            }
        }
    }
    //MARK:- Edit OnClick
    @IBAction func OnClickEdit(_ sender: Any) {
        
       
        object["title"] = self.txtTitle.text
        object["nameimage"] = self.txtName.text
        object.saveInBackground { (isUpdate, error) in
            if  isUpdate {
                print("Update Successfully...")
                self.navigationController?.popViewController(animated: true)
            }
        
        }
    }
    

}
