//
//  SqLiteDatabaseVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 04/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import GRDB

//let dbPool = try! DatabasePool(path: "/path/to/database.sqlite")


let databaseURL = try! FileManager.default
    .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    .appendingPathComponent("PersonDB.sqlite")
let dbQueue = try! DatabaseQueue(path: databaseURL.path)

class SqLiteDatabaseVC: UIViewController {

    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    //let dbQueue = try? DatabaseQueue(path: "/path/to/database.sqlite")
    //MARK:- View method
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Path ==> \(databaseURL.absoluteString)")
        // Do any additional setup after loading the view.
    }
    //MARK:- Custom Method
    @IBAction func OnClickSave(_ sender: Any) {
        createTable()
    }
    @IBAction func OnclickShowData(_ sender: Any) {
        let nextvc =  ObjectMainSB.instantiateViewController(withIdentifier: "ShowDataVC") as! ShowDataVC
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    
    func  createTable() {
        // Tablbe IF NOT EXISTS
        try! dbQueue.write { db in
            try db.execute("""
        CREATE TABLE IF NOT EXISTS Person (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name TEXT NOT NULL,
          city NOT NULL ,
          address NOT NULL)
        """)
            try! db.execute("""
        INSERT INTO Person (name, city, address)
        VALUES (?, ?, ?)
        """, arguments: [self.txtName.text,self.txtCity.text,self.txtAddress.text])
           // let parisId = db.lastInsertedRowID
        }
        self.txtCity.text = ""
        self.txtName.text = ""
        self.txtAddress.text = ""
    }
    
    //MARK: Database  method
    
}

