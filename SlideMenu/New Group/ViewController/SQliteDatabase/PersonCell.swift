//
//  PersonCell.swift
//  SlideMenu
//
//  Created by Dubond Mac on 04/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class PersonCell: UITableViewCell {

    @IBOutlet var lblCity: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet weak var lblAddres: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
