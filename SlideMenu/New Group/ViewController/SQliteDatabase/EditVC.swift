//
//  EditVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 05/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class EditVC: UIViewController {
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    var personInstance = PersonModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtAddress.text = personInstance.address
        self.txtCity.text = personInstance.city
        self.txtName.text = personInstance.name
    }
    func editData() {
        //let statement = try db.up("INSERT INTO persons (name) VALUES (?)")
        try dbQueue.write({ (db) in
            try! db.execute(
                "UPDATE Person SET name = :name,city = :city,address = :address WHERE id = :id",
                arguments: ["name": self.txtName.text,"city":self.txtCity.text,"address":self.txtAddress.text,"id": personInstance.id])
            self.navigationController?.popViewController(animated: true)
        })
    }
    @IBAction func OnClickEdit(_ sender: Any) {
        editData()
    }
    
}
