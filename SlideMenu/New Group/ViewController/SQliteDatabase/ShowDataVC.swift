//
//  ShowDataVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 04/10/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import GRDB

class PersonModel {
    var id: Int64?
    var name: String?
    var city: String?
    var address:String?
    

}
class ShowDataVC: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tblMainVC: UITableView!
    var  arrAllData: [PersonModel] = [PersonModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.arrAllData.removeAll()
        fetchAllRecord()

    }
    func fetchAllRecord() {
        let sqlQuery = "SELECT * FROM Person"
        let arrPerson = try dbQueue.read { db in
            //try Wine.fetchAll(db, "SELECT * FROM Person")
            let rows = try! Row.fetchAll(db,sqlQuery)
            for row in rows {
                let personInstance = PersonModel()
                print("\(row)")
                let name: String = row["name"]
                let city: String = row["city"]
                let address: String = row["address"]
                personInstance.address = address
                personInstance.city = city
                personInstance.name = name
                personInstance.id = row["id"]
                self.arrAllData.append(personInstance)
                print(" \(name)   \(city)    \(address)")
                }
        }
        print(self.arrAllData)
        DispatchQueue.main.async {
            self.tblMainVC.reloadData()
        }
    }
    
    func deleteRrcord(id:Int64) {
        print("id = \(id)")
        let arrPersonData = try! dbQueue.inDatabase({ (db)  in
           let row  = try db.execute("DELETE FROM Person WHERE id = ?", arguments:[id])
            print("\(row)")
        })
    
    }
    //MARK: Tableview Datasource & delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAllData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonCell") as! PersonCell
        cell.lblName.text = self.arrAllData[indexPath.row].name
        cell.lblCity.text = self.arrAllData[indexPath.row].city
        cell.lblAddres.text = self.arrAllData[indexPath.row].address
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextvc = ObjectMainSB.instantiateViewController(withIdentifier:"EditVC") as! EditVC
        nextvc.personInstance = self.arrAllData[indexPath.row]
        
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            print("ID == \(self.arrAllData[indexPath.row].id!)")
            self.deleteRrcord(id: self.arrAllData[indexPath.row].id!)
           // tableView.reloadRows(at: [indexPath], with: .automatic)
            self.arrAllData.remove(at: indexPath.row)
            let indexPath = IndexPath(item: indexPath.row, section: 0)
            self.tblMainVC.reloadData()
            //tableView.reloadRows(at: [indexPath], with: .top)
        }
       
    }



}
