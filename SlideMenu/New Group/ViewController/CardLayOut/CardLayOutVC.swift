//
//  CardLayOutVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 25/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class CardLayOutVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate{
  
    
    
    @IBOutlet weak var CollectionVC: UICollectionView!
    let arrImage:[UIImage] = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3")]
    let arrFoodName = ["Food1","Panjabi","Biriyani"]
    let arrfoodDescription = ["Good Food, Bad Family. Cersei set a tasty table, that could not be denied. They started with","Good2 Food, Bad Family. Cersei set a tasty table, that could not be denied. They started with","Good3 Food, Bad Family. Cersei set a tasty table, that could not be denied. They started with"]
   
    //MARK:- UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "CardLayout"
    }
    // Collection DataSouce & Delegate Meyhod
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return  self.arrImage.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"CardCell", for: indexPath) as! CardCell
        //
        cell.contentView.layer.cornerRadius = 8.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.backgroundColor = UIColor.magenta
        cell.contentView.layer.masksToBounds = true
        //
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 0.7
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        cell.lblfoodName.text = self.arrFoodName[indexPath.row]
        cell.lblDesciption.text = self.arrfoodDescription[indexPath.row]
        cell.imgCard.image = self.arrImage[indexPath.row]
        //
        
        return cell
    }
}
