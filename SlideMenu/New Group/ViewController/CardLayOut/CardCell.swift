//
//  CardCell.swift
//  SlideMenu
//
//  Created by Dubond Mac on 25/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class CardCell: UICollectionViewCell {
    
    @IBOutlet weak var lblfoodName: UILabel!
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var lblDesciption: UILabel!
    
}
