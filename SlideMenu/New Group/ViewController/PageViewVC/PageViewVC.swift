//
//  PageViewVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 26/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import CarbonKit

class PageViewVC: UIViewController,CarbonTabSwipeNavigationDelegate {
    //
    let  img1 = UIImage.init(named:"image9")
    let items = ["Medical","Sergery","Medicince","Apple","Orange"]

    //MARK:- UIView Method
    override func viewDidLoad() {
         super.viewDidLoad()
          let carbonTabSwipeNavigation = CarbonTabSwipeNavigation.init(items: items, delegate: self)
            carbonTabSwipeNavigation.setNormalColor(UIColor.red, font:UIFont.systemFont(ofSize: 10))
            carbonTabSwipeNavigation.setSelectedColor(UIColor.lightGray, font: UIFont.systemFont(ofSize: 10))
            carbonTabSwipeNavigation.setIndicatorColor(UIColor.orange)
            carbonTabSwipeNavigation.insert(intoRootViewController: self)
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
       // let instance = ObjectMainSB.instantiateViewController(withIdentifier:"PageViewVC") as! PageViewVC
     
        
        switch index {
        case 0:
            let autolayout =  ObjectMainSB.instantiateViewController(withIdentifier: "PageControllerVC") as! PageControllerVC
            autolayout.carbonNavigation = carbonTabSwipeNavigation
            return autolayout
        case 1:
            // do something similar, instantiate view controller, set navigation property
            let autolayout =  ObjectMainSB.instantiateViewController(withIdentifier: "AutolayOutVC") as! AutolayOutVC
            autolayout.carbonNavigation = carbonTabSwipeNavigation
            return autolayout
        case 2:
            // do something similar, instantiate view controller, set navigation property
            let autolayout =  ObjectMainSB.instantiateViewController(withIdentifier: "CollectionVC") as! CollectionVC
            autolayout.carbonNavigation = carbonTabSwipeNavigation
            return autolayout
            
        case 3:
            let autolayout =  ObjectMainSB.instantiateViewController(withIdentifier: "AutolayOutVC") as! AutolayOutVC
            autolayout.carbonNavigation = carbonTabSwipeNavigation
            return autolayout
        default:
            // do something similar, instantiate view controller, set navigation property
            let autolayout =  ObjectMainSB.instantiateViewController(withIdentifier: "AutolayOutVC") as! AutolayOutVC
            autolayout.carbonNavigation = carbonTabSwipeNavigation
            return autolayout
            
        }
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        print("===> \(index)")
        let indexNo = Int(index)
        self.title = self.items[indexNo]
    }
  

}


