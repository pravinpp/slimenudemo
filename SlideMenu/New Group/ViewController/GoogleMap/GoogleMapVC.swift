//
//  GoogleMapVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 28/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import GoogleMaps

class GoogleMapVC: UIViewController,GMSMapViewDelegate {

    //MARK:- View Method
    var arrlocation = [LocationName]()
    
//    var GMSMapViewInstance = GMSMapView()
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func loadView() {
        
        
        let loc1 = LocationName.init(cityName: "chainai", state: "Telangana, India", image:"c1" , lat:17.123184 , long:79.208824)
        let loc2 = LocationName.init(cityName: "Madhya Pradesh, India", state: "Madhya Pradesh, India", image:"c2" , lat:23.473324 , long:77.947998)
        let loc3 = LocationName.init(cityName: "Chhattisgarh", state: "Chhattisgarh, India", image:"c1" , lat:21.295132 , long:81.828232)
        let loc4 = LocationName.init(cityName: "Bhitarwar", state: "Bhitarwar, Madhya Pradesh, India", image:"c2" , lat:25.794033 , long:78.116531)
        let loc5 = LocationName.init(cityName: " Tripura", state: " Tripura, India", image:"c2" , lat:23.745127 , long:91.746826)
        let loc6 = LocationName.init(cityName: "Karnataka, India", state: "Karnataka, India", image:"c2" , lat:15.3772 , long:75.713890)
        let loc7 = LocationName.init(cityName: "Gujarat", state: "Gujarat, India", image:"c2" , lat:22.309425 , long:72.13290)
        //
        self.arrlocation.append(loc1)
        self.arrlocation.append(loc2)
        self.arrlocation.append(loc3)
        self.arrlocation.append(loc4)
        self.arrlocation.append(loc5)
        self.arrlocation.append(loc6)
        self.arrlocation.append(loc7)

        //
        let camera =  GMSCameraPosition.camera(withLatitude:  20.5937, longitude:78.9629, zoom: 0)
        let GMSMapViewInstance1 = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        var bounds = GMSCoordinateBounds()
         GMSMapViewInstance1.delegate = self
        self.view = GMSMapViewInstance1
        
        for   item in self.arrlocation {
            let marker = GMSMarker ()
            marker.position = .init(latitude: item.lat!, longitude: item.long!)
            marker.title = item.cityName!
            marker.snippet = item.state
            marker.accessibilityValue = item.image
            marker.icon = UIImage.init(named: "loc")
            bounds = bounds.includingCoordinate(marker.position)
            //marker.iconView = MarkerView.instancefromNib()
            marker.map = GMSMapViewInstance1
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        GMSMapViewInstance1.animate(with: update)
//        let camera =  GMSCameraPosition.camera(withLatitude:  23.033863, longitude:72.585022 , zoom: 10)
//        let GMSMapViewInstance1 = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//        self.view = GMSMapViewInstance1
//        GMSMapViewInstance1.settings.myLocationButton = true
//        marker.position = CLLocationCoordinate2D.init(latitude:23.033863 , longitude: 72.585022)
//        marker.iconView = MarkerView.instancefromNib()
       // marker.title = "Ahmedabad"
        //marker.snippet = "Gujarat ,India"
//        marker.map = GMSMapViewInstance
//        self.view = GMSMapViewInstance
    }
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
       // marker.title
        print("\(marker.title)")
        let markerinstance : CustomMarker =  CustomMarker.instancefromNib() as! CustomMarker
        if let city = marker.title {
            markerinstance.lblcity.text = city
        }
        if let imagename = marker.accessibilityValue {
            markerinstance.imgIcon.image = UIImage(named: imagename)
        }
        if let state = marker.snippet {
            markerinstance.lblstate.text = state
        }
        return markerinstance
        
    }
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        
        let markerinstance : CustomMarker =  CustomMarker.instancefromNib() as! CustomMarker
        //print()
        return markerinstance
    }
    

}
