//
//  Model.swift
//  SlideMenu
//
//  Created by Dubond Mac on 28/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import Foundation

class  LocationName {
    var cityName :String?
    var state :String?
    var image:String?
    var lat :Double?
    var long: Double?
    
    init(cityName:String,state:String, image: String,lat: Double,long: Double) {
        self.cityName = cityName
        self.state = state
        self.image = image
        self.lat = lat
        self.long = long
    }
    
    
}
