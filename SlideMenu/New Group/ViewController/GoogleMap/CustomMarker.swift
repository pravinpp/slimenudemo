//
//  CustomMarker.swift
//  SlideMenu
//
//  Created by Dubond Mac on 28/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit

class CustomMarker: UIView {
    @IBOutlet weak var lblcity: UILabel!
    @IBOutlet weak var lblstate: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    class func instancefromNib() -> UIView {
        return UINib.init(nibName:"CustomMarker", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
    
}
