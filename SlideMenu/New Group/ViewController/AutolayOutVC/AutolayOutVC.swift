//
//  AutolayOutVC.swift
//  SlideMenu
//
//  Created by Dubond Mac on 13/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import CarbonKit

class AutolayOutVC: UIViewController {
 weak var carbonNavigation : CarbonTabSwipeNavigation?
    
    //MARK:- UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func OnClick(_ sender: Any) {
//        let nextVC = ObjectMainSB.instantiateViewController(withIdentifier: "ListContactVC") as! ListContactVC
//        self.navigationController?.pushViewController(nextVC, animated: true)
        let nextVC = ObjectMainSB.instantiateViewController(withIdentifier:"ImageListVC") as! ImageListVC
        self.navigationController?.pushViewController(nextVC, animated: true)
        
        
    }
    @IBAction func OnClickChooseImage(_ sender: Any) {
        let nextVC = ObjectMainSB.instantiateViewController(withIdentifier:"ChoosePhotoVC") as! ChoosePhotoVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    @IBAction func OnClickFileManager(_ sender: Any) {
        let nextVC = ObjectMainSB.instantiateViewController(withIdentifier:"FileManager1") as! FileManager1
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
   
}
