//
//  APIResponse.swift
//  SlideMenu
//
//  Created by Dubond Mac on 17/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import Foundation
import ObjectMapper

class APIResponseData:Mappable{
    var  arrContact : [ContactNumber]?
    required init?(map: Map) {

    }
    func mapping(map: Map) {
        arrContact <- map["contacts"]
    }
}
class ContactNumber:Mappable {
    
    var name:String?
    var address:String?
    var email:String?
    var phone:String?
    var home:String?
    var office:String?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        self.name <- map["name"]
        self.email <- map["email"]
        self.address <- map["address"]
        self.address <- map["address"]
        self.phone <- map["phone.mobile"]
        self.office <- map["phone.office"]
        self.home <- map["phone.home"]
       // print(" == \(self.phone)")

    }
}
class  BookItem: Mappable {
    var title :String?
    var publisher:String?
    var imageURL: String?
    var author: [String]?
    required init?(map: Map) {
        
    }
     func mapping(map: Map) {
        self.title <- map["volumeInfo.title"]
        self.publisher <- map["volumeInfo.publisher"]
        self.author <- map["volumeInfo.authors"]
        self.imageURL <- map["volumeInfo.imageLinks.smallThumbnail"]
    }
}

class Item: Mappable {
    var arrItem: [BookItem]?
    required init?(map: Map) {
        
    }
     func mapping(map: Map) {
      arrItem <- map["items"]
    }
}
