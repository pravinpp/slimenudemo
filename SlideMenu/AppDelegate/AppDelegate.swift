//
//  AppDelegate.swift
//  SlideMenu
//
//  Created by Dubond Mac on 07/09/18.
//  Copyright © 2018 Procklinck. All rights reserved.
//

import UIKit
import Kingfisher
import UserNotifications
import GoogleMaps
import GooglePlaces
import SQLite3
import Parse
import IQKeyboardManagerSwift

let  ObjectMainSB = UIStoryboard.init(name: "Main", bundle: nil)
let  ObjectDesignSB = UIStoryboard.init(name:"DesignSB", bundle: nil)


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var tabarVC = UITabBarController()
    var window: UIWindow?
    var centerContainer: MMDrawerController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        //keyboard
        IQKeyboardManager.shared.enable = true
        let  centerViewController = ObjectMainSB.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let  leftViewController = ObjectMainSB.instantiateViewController(withIdentifier: "MenuListVC") as! MenuListVC
        GMSServices.provideAPIKey("AIzaSyDnprGc_tHzvk92H2xXBcYotlxhbEE48oo")
        GMSPlacesClient.provideAPIKey("AIzaSyDnprGc_tHzvk92H2xXBcYotlxhbEE48oo")
        //Welcome good 
        let leftSideNav = UINavigationController(rootViewController: leftViewController)
        let  centerNav = UINavigationController(rootViewController: centerViewController)        
        centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
        centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView;
        centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.panningCenterView;
        centerContainer?.setMaximumLeftDrawerWidth(250, animated: true, completion: nil)
        window!.rootViewController = centerContainer
        window!.makeKeyAndVisible()
        notificationRegster()
        // Parse Framework
       //https://MyAppName.herokuapp.com/parse
        
      //  https://parseapi.back4app.com
        let configuration = ParseClientConfiguration {
            $0.applicationId = "ONZoZuaHP9Qd3kMCVGdJJ9AJ7uvxnKDwuJAljXbh"
            $0.clientKey = "WxSemWrnYARbXdnf1w5RwPL35io1ax57e1iqk8XE"
            $0.server = "https://parseapi.back4app.com"
        }
        Parse.initialize(with: configuration)
        saveInstallationObject()
        return true
    }
    
    func saveInstallationObject(){
        
        
        if let installation = PFInstallation.current(){
            installation.saveInBackground {
                (success: Bool, error: Error?) in
                if (success) {
                    print("You have successfully connected your app to Back4App!")
                } else {
                    if let myError = error{
                        print(myError.localizedDescription)
                    }else{
                        print("Uknown error")
                    }
                }
            }
        }
    }

    func tabBarControllerVC()  {
        //
        let autolayout = ObjectMainSB.instantiateViewController(withIdentifier: "AutolayOutVC") as! AutolayOutVC
        let collection  = ObjectMainSB.instantiateViewController(withIdentifier: "TableVC") as!  TableVC
        let tablevc  =   ObjectMainSB.instantiateViewController(withIdentifier: "PageControllerVC") as! PageControllerVC
        let autolayout2 = ObjectMainSB.instantiateViewController(withIdentifier: "AutolayOutVC") as! AutolayOutVC
        let collection2  = ObjectMainSB.instantiateViewController(withIdentifier: "TableVC") as!  TableVC
        let tablevc2  =   ObjectMainSB.instantiateViewController(withIdentifier: "PageControllerVC") as! PageControllerVC
        //
        let  autolayoutItem = UITabBarItem.init(title: "Autolayout", image:UIImage.init(named: "home"), tag: 0)
        let  collectionItem = UITabBarItem.init(title: "CollectionVC", image:UIImage.init(named: "placeholder"), tag: 1)
        let  tablevcItem = UITabBarItem.init(title: "Pagecontroller", image:UIImage.init(named: "settings"), tag: 2)
        let  autolayoutItem2 = UITabBarItem.init(title: "Autolayout", image:UIImage.init(named: "home"), tag: 3)
        let  collectionItem2 = UITabBarItem.init(title: "CollectionVC", image:UIImage.init(named: "placeholder"), tag: 4)
        let  tablevcItem2 = UITabBarItem.init(title: "Pagecontroller", image:UIImage.init(named: "settings"), tag: 5)
        //
        autolayout.tabBarItem = autolayoutItem
        collection.tabBarItem = collectionItem
        tablevc.tabBarItem = tablevcItem
        autolayout2.tabBarItem = autolayoutItem2
        collection2.tabBarItem = collectionItem2
        tablevc2.tabBarItem = tablevcItem2
        let controllers = [autolayout,collection,tablevc]
        self.tabarVC.viewControllers = controllers
        //self.tabarVC.viewControllers = controllers.map { UINavigationController(rootViewController: $0)}
        self.tabarVC.tabBar.tintColor = UIColor.white
        self.tabarVC.tabBar.barTintColor = UIColor.red
        // Set ViewControllers
          self.tabarVC.setViewControllers(controllers, animated: false)
        // Set TabBar as RootViewController
        self.window!.rootViewController = self.tabarVC
        self.window!.makeKeyAndVisible()
    }
    func  notificationRegster(){
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().delegate = self
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge]) { (grant, error) in
                
                DispatchQueue.main.async {
                    print(" Register Successfulluy ")
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        else{
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
        }
        
    }
    //MARK:- Notification
    
 
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       // print("DeviceToken  ==")
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        print(token)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error ==> \(error)")
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge,.alert,.sound])

    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Dictionary==>\(response)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("\(userInfo)")
    }
    //MARK:-
    
   func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
       print(" Host ===> \(url.host)")
       print(" path ==> \(url.path)")
    
    let urlPath = url.path as String?
    if  (urlPath == "/inner") {
        let  innerNext =  ObjectDesignSB.instantiateViewController(withIdentifier:"DeeplinkVC") as! DeeplinkVC
        self.window?.rootViewController = innerNext
        self.window?.makeKeyAndVisible()
    }
    return  true
    
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
 


}

